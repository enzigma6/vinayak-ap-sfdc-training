function validateCandidateForm() {
    var field = document.querySelector('[id$="age"]');
    var error = document.getElementById('errorAge');
    
    if(field.value == null){
        error.style.display = 'block';
        return false;
    } else {
        error.style.display = 'none';
    }
}

function validateJobForm() {
    var field = document.querySelector('[id$="description"]');
    var error = document.getElementById('errorDescription');
    
    if(field.value == null || field.value == ''){
        error.style.display = 'block';
        return false;
    } else {
        error.style.display = 'none';
    }
}