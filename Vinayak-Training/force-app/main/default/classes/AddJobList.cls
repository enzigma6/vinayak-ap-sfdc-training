public class AddJobList {
    public void addJobs() {
        List<Job_Vinayak__c> jobList = new List<Job_Vinayak__c> {
            new Job_Vinayak__c(
                Job_Title__c = 'Jr. Software Engineer',
                Manager__c = '0035g000004EIVt',
                Number_of_Positions__c = 12
                //Required_Skills__c = 'PHP',
                //Qualifications_Required__c = 'B.E.',
                //Certifications_Required__c = 'SAS'
            ),
            new Job_Vinayak__c(
                Job_Title__c = 'Sr. Software Engineer',
                Manager__c = '0035g000004EIVt',
                Number_of_Positions__c = 8
                //Required_Skills__c = 'PHP',
                //Qualifications_Required__c = 'B.E.',
                //Certifications_Required__c = 'SAS'
            )
        };
           
        insert jobList;
    }
}