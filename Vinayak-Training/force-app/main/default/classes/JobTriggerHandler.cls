public class JobTriggerHandler {
    
    //To display error if user is deleting a job having status as active
    public static void preventDeletion(List<Job_Vinayak__c> newJob) {
        for(Job_Vinayak__c job : newJob) {
            if(job.Active__c) {
                job.addError('This Job is Active and cannot be deleted');
            }
        }
    }
    
    
    //To change the job status according to the number of hired candidates
    public static void changeJobStatus(List<Job_Vinayak__c> updatedJob) {
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        for(Job_Vinayak__c job : updatedJob) {
            if(job.Hired_Applicants__c == job.Number_of_Positions__c && job.Active__c == true) {
                job.Active__c = false; 
                messages.addAll(sendCustomEmail(job.LastModifiedDate));
            } else if (job.Hired_Applicants__c < job.Number_of_Positions__c && job.Active__c == false) {
                job.Active__c = true;
            }
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }
    
    
    //To send a confirmation email to candidate if the status is 'Hired'
    public static List<Messaging.SingleEmailMessage> sendCustomEmail(Datetime completionDate) {
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { 'vinayak.patil.salesforce@gmail.com' };
        message.subject = 'Hiring Process Completed';
        message.plainTextBody = 'All required candidate has been hired for this job on ' + completionDate;
        
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        return messages;
    }
}