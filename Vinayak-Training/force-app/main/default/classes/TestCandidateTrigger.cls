@isTest
private class TestCandidateTrigger {

    @isTest
    static void testExpectedsalaryWithEmptyField() {
        Candidate_Vinayak__c candidate = TestDataFactory.createJobAndCandidate(true, null);
            
        Test.startTest();
        Database.SaveResult resultList = Database.insert(candidate, false);
        Test.stopTest();
        
        //Verify if we get error meassage or not  
        System.assert(!resultList.isSuccess());
        System.assert(resultList.getErrors().size() > 0);
        System.assertEquals('Please Enter Expected Salary.', resultList.getErrors()[0].getMessage());
    }
    
    @isTest
    static void testExpectedSalaryWithValidValue() {
        Candidate_Vinayak__c candidate = TestDataFactory.createJobAndCandidate(true, 10000);
        
        Test.startTest();
        Database.SaveResult resultList = Database.insert(candidate, false);
        Test.stopTest();
        
        //Verify if the record is successfully created or not
        System.assert(resultList.isSuccess());
    }
    
    @isTest
    static void testExpectedSalryWithInvalidValue() {
    	Candidate_Vinayak__c candidate = TestDataFactory.createJobAndCandidate(true, -10000);
        
        Test.startTest();
        Database.SaveResult resultList = Database.insert(candidate, false);
        Test.stopTest();
        
        //Verify if the record is successfully created or not
        System.assert(!resultList.isSuccess());
        System.assert(resultList.getErrors().size() > 0);
        System.assertEquals('Salary Cannot Be Negative.', resultList.getErrors()[0].getMessage());
    }
    
    @isTest
    static void testCheckJobStatus() {
        Candidate_Vinayak__c candidate = TestDataFactory.createJobAndCandidate(false, 10000);
            
        Test.startTest();
        Database.SaveResult resultList = Database.insert(candidate, false);
        Test.stopTest();
        
        //Verify if we get error meassage or not  
        System.assert(!resultList.isSuccess());
        System.assert(resultList.getErrors().size() > 0);
        System.assertEquals('This Job is not active. You can not apply for this job.', resultList.getErrors()[0].getMessage());
    }
    
    @isTest
    static void testApplicationDate() {
        Candidate_Vinayak__c candidate = TestDataFactory.createJobAndCandidate(true, 10000);
            
        Test.startTest();
        Database.SaveResult resultList = Database.insert(candidate, false);
        Test.stopTest();
        
        System.assert(resultList.isSuccess());
        System.assertEquals(candidate.Application_Date__c, candidate.CreatedDate);
    }
}