@isTest
global class ParkServiceMock implements WebServiceMock {
    
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            ParkService.byCountryResponse response_x = new ParkService.byCountryResponse();
            List<String> listOfAvailableParks = new List<String> {'Park-A','Park-B','Park-C'};
                response_x.return_x = listOfAvailableParks;
            
            response.put('response_x', response_x);
        }
}