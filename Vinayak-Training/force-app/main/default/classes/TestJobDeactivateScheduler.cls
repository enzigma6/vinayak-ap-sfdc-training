@isTest
public class TestJobDeactivateScheduler {

    public static string cronExp = '0 0 0 20 4 ? 2022';
    //public static string cronExp = System.now().format();
    
    @isTest
    public static void testScheduledJob() {
        List<Job_Vinayak__c> jobList = new List<Job_Vinayak__c>();
        Date expiryDate = Date.today().addDays(-10);
        
        Contact contact = new Contact(Lastname = 'Williamson');
        
        for (Integer i=1; i<10; i++) {
            Job_Vinayak__c job = new Job_Vinayak__c(
                Job_Title__c = 'Test Job 0' + i,
                Manager__c = contact.Id,
                Number_of_Positions__c = 4,
            	Active__c = true,
                Expires_On__c = expiryDate
            );
            jobList.add(job);
        }
        insert jobList;
        
        Test.startTest();
        String jobId = System.schedule('ScheduledApexTest', cronExp, new JobDeactivateScheduler());
        for(Job_Vinayak__c job : jobList) {
            System.assertEquals(true, job.Active__c);
        }
        Test.stopTest();
        
        for(Job_Vinayak__c job : jobList) {
            System.assertEquals(false, job.Active__c, 'Job is not deactivated');
        }
        
        
        
        
        
        /***********************
        Map<Id, Job_Vinayak__c> jobMap = new Map<Id, Job_Vinayak__c>(jobList);
        List<Id> jobIds = new List<Id>(jobMap.keySet());

        Test.startTest();
        String jobId = System.schedule('ScheduledApexTest', cronExp, new JobDeactivateScheduler());
        List<Task> listTask = [SELECT Id FROM Task WHERE WhatId IN :jobIds];
        
        System.assertEquals(0, listTask.size(), 'Tasks exist before job has run');
        Test.stopTest();
        
        listTask = [SELECT Id FROM Task WHERE WhatId IN :jobIds];
        System.assertEquals(jobIds.size(), listTask.size(), 'Tasks were not created');
        ***********************/
        
    }
}