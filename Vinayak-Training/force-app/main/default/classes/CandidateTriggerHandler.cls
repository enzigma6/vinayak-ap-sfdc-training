public class CandidateTriggerHandler {
    
    public static void checkExpectedSalary( List <Candidate_Vinayak__c> newList ) {
        for(Candidate_Vinayak__c candidate : newList) {
            if(candidate.Expected_Salary__c == null) {
                candidate.addError('Please Enter Expected Salary.');
            }
            if(candidate.Expected_Salary__c < 0) {
                candidate.addError('Salary Cannot Be Negative.');
            }
        }
    }
    
    public static void checkJobStatus( List <Candidate_Vinayak__c> newList, Map <Id, Candidate_Vinayak__c> oldMap ) {
        Map<Id, Job_Vinayak__c> newJob = new Map<Id, Job_Vinayak__c>([SELECT Id, Active__c FROM Job_Vinayak__c 
                                                                      WHERE Active__c = false]);
        for(Job_Vinayak__c job : newJob.values()) {
            System.debug('Map => ' + job);
        }
        for(Candidate_Vinayak__c candidate : newList) {
            if(newJob.get(candidate.Job__c) != null && candidate.Job__c != oldMap.get(candidate.Id).Job__c) {
                candidate.addError('This Job is not active. You can not apply for this job.');
            }
        }
    }
    
    public static void insertApplicationDate(List<Candidate_Vinayak__c> newList) {
        for(Candidate_Vinayak__c candidate : newList) {
            if(candidate.Application_Date__c != candidate.CreatedDate || candidate.Application_Date__c == null) {
                candidate.Application_Date__c = System.today();
                //candidate.Application_Date__c = DATEVALUE(candidate.CreatedDate);
            }
        }
    }
}