public class TaskParentChildRelation {
    
    public static void getAllChilds() {
        List<Account> accChildList = [SELECT Id, Name FROM Account WHERE ParentId = '0015g00000ASq5I'];
        
        for(Account accChild : accChildList) {
            System.debug('AccChild => ' + accChild);
        }
    }
}