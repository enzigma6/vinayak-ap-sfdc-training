public class ApexSharingJobController {
    
    public List<Job_Vinayak__c> jobRecords {get; set;}
    public List<Candidate_Vinayak__c> candidateRecords {get; set;}
    public String selectedJob {get; set;}
    
    public ApexSharingJobController() {
        jobRecords = [SELECT 
                      Id,
                      Name, 
                      Job_Title__c,
                      Number_of_Positions__c, 
                      Salary_Offered__c,
                      Required_Skills__c, 
                      Active__c 
                      FROM Job_Vinayak__c];
    }
    
    public List<SelectOption> getJobOptions() {
        List<SelectOption> jobOptions = new List<SelectOption>();
        for(Job_Vinayak__c job : jobRecords) {
            jobOptions.add(new SelectOption(job.Id, job.Job_Title__c));
        }
        return jobOptions;
    }
    
    public void fetchJobRecords() {
        candidateRecords = [SELECT 
                            Name,
                            Full_Name__c,
                            Email__c,
                            Country__c,
                            Expected_Salary__c,
                            Application_Date__c,
                            Status__c
                            FROM Candidate_Vinayak__c 
                            WHERE Job__c =: selectedJob];
    }
}