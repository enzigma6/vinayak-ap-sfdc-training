@isTest
private class AnimalLocatorTest {
    
    @isTest
    static  void testGetCallout() {
        Test.SetMock(HttpCallOutMock.class, new AnimalLocatorMock());
        string result=AnimalLocator.getAnimalNameById(1);
        string expectedResult = 'Cow';
        System.assertEquals(result, expectedResult);
    }
}