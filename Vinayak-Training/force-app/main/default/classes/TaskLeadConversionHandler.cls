public class TaskLeadConversionHandler {
    public static List<String> oldEmail = new List<String>();
    public static List<Lead> emailList = new List<Lead>();
    
    //To fetch and store duplicate email ids if present
    public static void createLead(List<Lead> newList) {
        for(Lead lead : newList) {
            oldEmail.add(lead.Email);
        }
        emailList = [SELECT Email FROM Lead WHERE Email IN: oldEmail];
        System.debug('***EMAIL*** => ' + emailList);
    }
    
    //To convert lead if the email id is already present in the database
    public static void convertLead(List<Lead> newList) {
        for(Lead lead : newList) {
            if(emailList.isEmpty()) {
                return;
                
            } else {
                Database.LeadConvert leadConvert = new Database.LeadConvert();
                leadConvert.setLeadId(lead.id);
                 
                LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
                leadConvert.setConvertedStatus(convertStatus.MasterLabel);
                
                Database.LeadConvertResult lcr = Database.convertLead(leadConvert);
            }
        }
    }
}