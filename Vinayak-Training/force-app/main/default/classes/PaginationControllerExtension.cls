public class PaginationControllerExtension {
    public PaginationControllerExtension( ApexPages.StandardSetController stdController ) {
        stdController.setPageSize(5);
    }
}