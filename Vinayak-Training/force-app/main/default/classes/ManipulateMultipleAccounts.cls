public class ManipulateMultipleAccounts {
    public static void addAccounts() {
        List<Account> accList = new List<Account> {
            new Account(
                Name = 'Buggati',
                ParentId = '0015g00000ASq5I'
            ),
            new Account(
                Name = 'Ducati',
                ParentId = '0015g00000ASq5I'
            )
        };
        insert accList;
    }
    
    public static void deleteAccounts() {
        List<Account> accIds = [SELECT Id FROM Account WHERE Name = 'Buggati' OR Name = 'Ducati'];
        delete accIds;
    }
}