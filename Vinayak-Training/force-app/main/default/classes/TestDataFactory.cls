@isTest
public class TestDataFactory {
    
    public static Candidate_Vinayak__c createJobAndCandidate(Boolean isJobActive, Integer expectedSalary) {
        
        Job_Vinayak__c job = new Job_Vinayak__c(
                Job_Title__c = 'Jr. Software Engineer',
                Manager__c = '0035g000004EIVt',
                Number_of_Positions__c = 12,
            	Active__c = isJobActive
            );
        insert job;
        
        Candidate_Vinayak__c candidate = new Candidate_Vinayak__c(
            Salutation__c = 'Ms.',
            First_Name__c = 'Olivia',
            Last_Name__c = 'Barnes',
            Email__c = 'olivia.barnes@example.com',
            Job__c = job.Id,
            Status__c = 'Applied',
            Expected_Salary__c = expectedSalary
        );
        return candidate;
    }
}