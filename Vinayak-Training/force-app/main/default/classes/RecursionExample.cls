public class RecursionExample {
    public static void insertContact(List<Contact> newList) {
        List<Contact> contact = new List<Contact>();
        
        for(Contact cont: newList) {
            cont.Department = 'Technical Support';
            contact.add(cont);
        }
        
        if(!contact.isEmpty()) {
            //System.debug(contact);
            insert contact;
        }
    }
}