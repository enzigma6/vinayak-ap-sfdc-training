public class SendEmail {
    private static Set<Id> candidateIds = new Set<Id>();
    
    public static void checkIfInsertingData(List<Candidate_Vinayak__c> newList) {
        for(Candidate_Vinayak__c candidate : newList) {
            if(candidate.Status__c == 'Hired') {
                candidateIds.add(candidate.Id);
        	}
        }
        if(candidateIds.size() > 0) {
        	SendToCandidate(candidateIds);        
        }
    }
    
    
    public static void checkIfUpdatingData(List<Candidate_Vinayak__c> newList, Map<Id,Candidate_Vinayak__c> oldMap) {
        for(Candidate_Vinayak__c candidate : newList) {
            if(candidate.Status__c == 'Hired' && candidate.Status__c != oldMap.get(candidate.Id).Status__c) {
                candidateIds.add(candidate.Id);
        	}
        }
        if(candidateIds.size() > 0) {
        	SendToCandidate(candidateIds);        
        }
    }
    
    
    
    @Future(callout=true)
	public static void SendToCandidate(Set<Id> Ids) {
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        List<Candidate_Vinayak__c> candidateList = [SELECT 
                                                 	Id, 
                                                    Full_Name__c,
                                                 	Email__c,
                                                    Job__c,
                                                    LastModifiedDate
                                                 	FROM Candidate_Vinayak__c 
                                                 	WHERE Id in : Ids];
        
        for(Candidate_Vinayak__c candidate : candidateList) {
            PageReference pgRef = page.CandidateDetailPDF;
            pgRef.getParameters().put('id', candidate.id);
            Blob body = pgRef.getContent();
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            
            attachment.setFileName('Candidate Details.pdf');
            attachment.setBody(body);
            
            String[] address = new String[]{candidate.Email__c};
            email.setToAddresses(address);
            
            email.setSubject('Candidate Application Form Details');
            email.setPlainTextBody('Hello ' + candidate.Full_Name__c + ',' +
                                   '\n\nCongratulations !' +
                                   '\nYou are hired for the job ' + candidate.Job__c +
                                   'on ' + candidate.LastModifiedDate
                                  );
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{attachment});
            emailList.add(email);
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(emailList);
    }
}