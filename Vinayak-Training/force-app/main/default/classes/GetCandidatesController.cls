public class GetCandidatesController {
 
    public List<sObject> records {get; set;}
    public List<String> fields {get; set;}
  	public List<Candidate_Vinayak__c> candidateRecords {get; set;}
    
    public GetCandidatesController() {
        Id id= ApexPages.currentPage().getParameters().get('id');
        records = [SELECT 
                   Id,
                   Name, 
                   Job_Title__c, 
                   Manager__c, 
                   Number_of_Positions__c, 
                   Salary_Offered__c,
                   Required_Skills__c, 
                   Active__c 
                   FROM Job_Vinayak__c WHERE Id =: id];
        
        fields = new List<String>{'Name', 'Job_Title__c', 'Manager__c', 'Number_of_Positions__c', 
            					  'Salary_Offered__c', 'Required_Skills__c', 'Active__c'};
     
        candidateRecords = [SELECT 
                            Name,
                            Full_Name__c,
                  			Email__c,
                  			Country__c,
                  			Application_Date__c,
                  			Status__c
                  			FROM Candidate_Vinayak__c 
                            WHERE Job__c =: records];
    }
}