public class TaskGetCandidateListController {
    
 	public List<Job_Vinayak__c> jobRecords {get; set;}
  	public List<Candidate_Vinayak__c> candidateRecords {get; set;}
    public String selectedJob {get; set;}
    
    public TaskGetCandidateListController() {
        jobRecords = [SELECT Id, Job_Title__c FROM Job_Vinayak__c];
    }
    
    public List<SelectOption> getJobOptions() {
    	List<SelectOption> jobOptions = new List<SelectOption>();
        for(Job_Vinayak__c job : jobRecords) {
            jobOptions.add(new SelectOption(job.Id, job.Job_Title__c));
        }
        return jobOptions;
	}
    
    public void fetchCandidateList() {
        candidateRecords = [SELECT 
                            Name,
                            Full_Name__c,
                  			Email__c,
                  			Country__c,
                            Expected_Salary__c,
                  			Application_Date__c,
                  			Status__c
                  			FROM Candidate_Vinayak__c 
                            WHERE Job__c =: selectedJob];
    }
}