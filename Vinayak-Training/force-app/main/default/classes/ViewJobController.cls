global with sharing class ViewJobController {
    
    public static Id jobId {get;set;}
	public List<Job_Vinayak__c> jobRecords {get; set;}
    public Candidate_Vinayak__c candidate {get; set;}
    
  	public static List<sObject> candidateRecords {get; set;}
    public static List<String> candidateFields {get; set;}

    public ViewJobController() {
        candidate = new Candidate_Vinayak__c();
		jobRecords = [SELECT 
                      Id,
                      Name, 
                      Job_Title__c,
                      Number_of_Positions__c, 
                      Salary_Offered__c,
                      Required_Skills__c, 
                      Active__c 
                      FROM Job_Vinayak__c];
	}
   
    public static PageReference fetchCandidates() {
        // Id id= ApexPages.currentPage().getParameters().get('id');;
        candidateRecords = [SELECT 
                   			Id,
                            Name, 
                            Full_Name__c, 
                            Email__c, 
                            Country__c, 
                            Application_Date__c,
                            Status__c 
                            FROM Candidate_Vinayak__c 
                            WHERE Job__c =: jobId];
        
        candidateFields = new List<String> {'Name', 'Full_Name__c', 'Email__c', 'Country__c', 
            					   			'Application_Date__c', 'Status__c'};
        return Null;
    }
    
    public void insertCandidate(){
        System.debug('***INSERTING CANDIDATE***');
        System.debug('***CANDIDATE OBJ*** => ' + candidate);
        insert candidate;
    }
    
    
    public static String jid { get; set; }
    public static Job_Vinayak__c job { get; set; }
     
    @RemoteAction
    global static void increaseCount(String jid) {
        System.debug('***REMOTE ACTION INVOKED***');
        System.debug('***JobId*** => ' + jid);
        
        job = [SELECT Id, Number_of_Positions__c
        	   FROM Job_Vinayak__c WHERE Id = :jid];
        System.debug('***JOB*** => ' + job);
        
        job.Number_of_Positions__c = job.Number_of_Positions__c + 1;
        System.debug('No. of Positions => ' + job.Number_of_Positions__c);
        update job;
    }
}