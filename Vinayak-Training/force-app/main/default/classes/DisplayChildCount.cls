public class DisplayChildCount {
    private static Boolean isCreating = false;
    //private static Boolean isDeleting = false;
    
    public static void onAfterInsert(List<Account> newList) {
        isCreating = true;
        Set<Id> accIds = new Set<Id>();
        Set<Id> parentIds = new Set<Id>();
        
        for(Account acc : newList) {
            if(acc.ParentId != null) {
                accIds.add(acc.Id);
                parentIds.add(acc.ParentId);
            }
        }
        calculateChildCount(accIds, parentIds);
        isCreating = false;
    }
    
    
    public static void onAfterUpdate(List<Account> newList, Map<Id,Account> oldMap) {
        isCreating = true;
        Set<Id> accIds = new Set<Id>();
        Set<Id> parentIds = new Set<Id>();
        
        for(Account acc : newList) {
            if(acc.ParentId != null && acc.ParentId != oldMap.get(acc.Id).ParentId) {
                accIds.add(acc.Id);
                parentIds.add(acc.ParentId);
            }
        }
        calculateChildCount(accIds, parentIds);
        isCreating = false;
    }
    
    
    public static void onAfterDelete(List<Account> oldList) {
        //isDeleting = true;
        Set<Id> accIds = new Set<Id>();
        Set<Id> parentIds = new Set<Id>();
        
        for(Account acc : oldList) {
            if(acc.ParentId != null) {
                accIds.add(acc.Id);
                parentIds.add(acc.ParentId);
            }
        }
        calculateChildCount(accIds, parentIds);
        //isDeleting = false;
    }
    
    
    public static void calculateChildCount(Set<Id> accIds, Set<Id> parentIds) {
        List<Account> accList = [SELECT Id, Total_Child__c FROM Account WHERE Id IN : parentIds];
        System.debug('accList => ' + accList);
        
        for(Account acc : accList) {
            if(acc.Total_Child__c != null) {
                if (isCreating) {
                    System.debug('Total Child Before => ' + acc.Total_Child__c);
                    System.debug('***Creating***');
                    acc.Total_Child__c = acc.Total_Child__c + accIds.size();
                    System.debug('Total Child After => ' + acc.Total_Child__c);
                } else if (!isCreating) {
                    System.debug('Total Child Before => ' + acc.Total_Child__c);
                    System.debug('***Deleting***');
                    acc.Total_Child__c = acc.Total_Child__c - accIds.size();
                    System.debug('Total Child After => ' + acc.Total_Child__c);
                }   
            } else if (isCreating) {
                System.debug('Total Child Before => ' + acc.Total_Child__c);
                System.debug('***Deleting***');
                acc.Total_Child__c = accIds.size();
                System.debug('Total Child After => ' + acc.Total_Child__c);
            }
        }
        update accList;  
    }
}