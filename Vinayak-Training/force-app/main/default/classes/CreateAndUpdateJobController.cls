public class CreateAndUpdateJobController {
	
    public Job_Vinayak__c job {get; private set;}
    
    public CreateAndUpdateJobController() {
        Id id=ApexPages.currentPage().getParameters().get('id');
        
        if(id==null) {
            job = new Job_Vinayak__c();
        } else {
            job = [SELECT 
            		Job_Title__c,
                    Manager__c,
                    Salary_Offered__c,
                    Number_of_Positions__c,
                    Description__c,
                    Required_Skills__c,
                    Qualifications_Required__c,
                    Certifications_Required__c,
                    Expires_On__c,
                    Active__c
                    FROM Job_Vinayak__c WHERE Id=:id 
                  ];
        }
    }
    
    public PageReference saveJob() {
        try {
            upsert(job);
        }
        catch (System.DmlException err) {
            ApexPages.addMessages(err);
            return null;
        }
        PageReference redirectSuccess = new ApexPages.StandardController(job).view();
        return redirectSuccess;
    }
}