public class ParkLocator {
    
    public static String[] country(String country){
        ParkService.ParksImplPort parks = new ParkService.ParksImplPort();
        String[] parksAvailable = parks.byCountry(country);
        
        return parksAvailable;
    }
}