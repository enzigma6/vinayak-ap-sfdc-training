public class JobDeactivateScheduler implements Schedulable{
   public void execute(SchedulableContext sc){
       
       List<Job_Vinayak__c> jobList = [SELECT 
                                       Id, 
                                       Active__c, 
                                       Expires_On__c
                                       FROM Job_Vinayak__c WHERE Expires_On__c < Today];
       
       for(Job_Vinayak__c job : jobList){
           job.Active__c = false;
       }
       update jobList;
       System.debug('***SCHEDULAR METHOD CALLED***');
   }
}