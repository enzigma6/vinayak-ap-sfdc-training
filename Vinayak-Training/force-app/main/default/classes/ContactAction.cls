public class ContactAction {
    
    @InvocableMethod
    public static void autoPopulateContactAddressField(List<Id> contactIds) {
        List<Contact> contactToBeUpdated = new List<Contact>();
        List<Messaging.SingleEmailMessage> messageList = new List<Messaging.SingleEmailMessage>();
        
        List<Contact> contactList = [SELECT 
                                  Name,
                                  Email,
                                  CreatedDate,
                                  AccountId,
                                  Account.BillingStreet,
                                  Account.BillingCity,
                                  Account.BillingState,
                                  Account.BillingCountry,
                                  Account.BillingPostalCode
                                  FROM Contact WHERE Id IN: contactIds];
        
        for(Contact contact : contactList) {
            System.debug('***INSIDE LOOP***');
            contact.MailingStreet = contact.Account.BillingStreet;
            contact.MailingCity = contact.Account.BillingCity;
            contact.MailingState = contact.Account.BillingState;
            contact.MailingCountry = contact.Account.BillingCountry;
            contact.MailingPostalCode = contact.Account.BillingPostalCode;
            
            contactToBeUpdated.add(contact);
            messageList.addAll(sendEmailToContact(contact));
        }
        
        update contactToBeUpdated;
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messageList);
    }
 
    
    public static List<Messaging.SingleEmailMessage> sendEmailToContact(Contact contact) {
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { contact.Email };
        message.subject = 'Record Created';
        message.plainTextBody = 'Hello ' + contact.Name + ',' + '\n\nYour contact record is successfully created on ' + contact.CreatedDate;
        
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        return messages; 
    }
}