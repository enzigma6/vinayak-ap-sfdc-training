public class CreateAndUpdateCandidateController {
    
    public Candidate_Vinayak__c candidate {get; private set;}
    
    public CreateAndUpdateCandidateController() {
        Id id=ApexPages.currentPage().getParameters().get('id');
        
        if(id==null) {
           candidate = new Candidate_Vinayak__c();
        } else {
            candidate = [SELECT 
                         Salutation__c,
                         First_Name__c,
                         Last_Name__c,
                         Email__c,
                         DOB__c,
                         Age__c,
                         Country__c,
                         State__c,
                         Job__c,
                         Application_Date__c,
                         Expected_Salary__c,
                         Status__c
                         FROM Candidate_Vinayak__c WHERE Id=:id 
                        ];
        }
    }
    
    public PageReference saveCandidate() {
        try {
            upsert(candidate);
        }
        catch (System.DmlException err) {
            ApexPages.addMessages(err);
            return null;
        }
        PageReference redirectSuccess = new ApexPages.StandardController(candidate).view();
        return redirectSuccess;
    }
}