@isTest
private class ParkLocatorTest{
    
    @isTest 
    static void testParkLocator() {
        Test.setMock(WebServiceMock.class, new ParkServiceMock());
        String[] arrAvailableParks = ParkLocator.country('India');
        
        System.assertEquals('Park-A', arrAvailableParks[0]);
        System.assertEquals(3, arrAvailableParks.size(), 'The array should only contain 3 items.');
    }
}