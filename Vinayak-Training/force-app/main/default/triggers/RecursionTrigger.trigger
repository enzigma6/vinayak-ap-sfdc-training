trigger RecursionTrigger on Contact (before insert) {
    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            /****
            if(!PreventRecursion.firstCall) {
                PreventRecursion.firstCall = true;
                RecursionExample.insertContact(Trigger.New);
            }
            ****/
            
            RecursionExample.insertContact(Trigger.New);
        }
    }
}