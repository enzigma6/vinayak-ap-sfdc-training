trigger ApexTriggerExample 
    on Candidate_Vinayak__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            System.debug('***BEFORE INSERT*** Trigger.old => ' + Trigger.old);
            System.debug('***BEFORE INSERT*** Trigger.new => ' + Trigger.new);
            System.debug('***BEFORE INSERT*** Trigger.oldMap => ' + Trigger.oldMap);
            System.debug('***BEFORE INSERT*** Trigger.newMap => ' + Trigger.newMap);
        }
        if(Trigger.isAfter) {
            System.debug('***AFTER INSERT*** Trigger.old => ' + Trigger.old);
            System.debug('***AFTER INSERT*** Trigger.new => ' + Trigger.new);
            System.debug('***AFTER INSERT*** Trigger.oldMap => ' + Trigger.oldMap);
            System.debug('***AFTER INSERT*** Trigger.newMap => ' + Trigger.newMap);
        }
    }
    
        
    if(Trigger.isUpdate) {
        if(Trigger.isBefore) {
            System.debug('***BEFORE UPDATE*** Trigger.old => ' + Trigger.old);
            System.debug('***BEFORE UPDATE*** Trigger.new => ' + Trigger.new);
            System.debug('***BEFORE UPDATE*** Trigger.oldMap => ' + Trigger.oldMap);
            System.debug('***BEFORE UPDATE*** Trigger.newMap => ' + Trigger.newMap);
        }
        if(Trigger.isAfter) {
            System.debug('***AFTER UPDATE*** Trigger.old => ' + Trigger.old);
            System.debug('***AFTER UPDATE*** Trigger.new => ' + Trigger.new);
            System.debug('***AFTER UPDATE*** Trigger.oldMap => ' + Trigger.oldMap);
            System.debug('***AFTER UPDATE*** Trigger.newMap => ' + Trigger.newMap);
        }
    }
        
        
    if(Trigger.isDelete) {
        if(Trigger.isBefore) {
            System.debug('***BEFORE DELETE*** Trigger.old => ' + Trigger.old);
            System.debug('***BEFORE DELETE*** Trigger.new => ' + Trigger.new);
            System.debug('***BEFORE DELETE*** Trigger.oldMap => ' + Trigger.oldMap);
            System.debug('***BEFORE DELETE*** Trigger.newMap => ' + Trigger.newMap);
        }
        if(Trigger.isAfter) {
            System.debug('***AFTER DELETE*** Trigger.old => ' + Trigger.old);
            System.debug('***AFTER DELETE*** Trigger.new => ' + Trigger.new);
            System.debug('***AFTER DELETE*** Trigger.oldMap => ' + Trigger.oldMap);
            System.debug('***AFTER DELETE*** Trigger.newMap => ' + Trigger.newMap);
        }
    }
        
        
    if(Trigger.isUndelete) {
        if(Trigger.isAfter) {
            System.debug('***AFTER UNDELETE*** Trigger.old => ' + Trigger.old);
            System.debug('***AFTER UNDELETE*** Trigger.new => ' + Trigger.new);
            System.debug('***AFTER UNDELETE*** Trigger.oldMap => ' + Trigger.oldMap);
            System.debug('***AFTER UNDELETE*** Trigger.newMap => ' + Trigger.newMap);
        }
    }
}