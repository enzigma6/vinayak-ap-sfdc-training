trigger DisplayChildCountTrigger on Account (after insert, after update, after delete) {
    
    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            DisplayChildCount.onAfterInsert(Trigger.New);
        }
        else if(Trigger.isUpdate) {
            DisplayChildCount.onAfterUpdate(Trigger.New, Trigger.oldMap);
        }
        else if(Trigger.isDelete) {
            DisplayChildCount.onAfterDelete(Trigger.Old);
        }
    }
}