trigger TaskLeadConversion on Lead (before insert, after insert) {
    
    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            TaskLeadConversionHandler.createLead(Trigger.New);
        }
        if(Trigger.isAfter) {
            TaskLeadConversionHandler.convertLead(Trigger.New);
        }
    }
}