trigger JobTrigger on Job_Vinayak__c (before delete, before update) {
    
    if(Trigger.isBefore) {
        if(Trigger.isDelete) {
            Job_Trigger_Handler_Custom_Setting__c jobCustomSetting = Job_Trigger_Handler_Custom_Setting__c.getInstance(UserInfo.getUserId());
            //If the user can bypass the trigger, return and do not continue
            if(jobCustomSetting.Bypass_Job_Trigger__c) {
                return;
            }
            JobTriggerHandler.preventDeletion(Trigger.Old);
        }
    }
    
    if(Trigger.isBefore) {
        if(Trigger.isUpdate) {
            JobTriggerHandler.changeJobStatus(Trigger.New);
        }
    }
}