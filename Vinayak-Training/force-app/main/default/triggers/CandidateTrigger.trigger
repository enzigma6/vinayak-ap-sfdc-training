trigger CandidateTrigger on Candidate_Vinayak__c (before insert, before update) {
    if(Trigger.isBefore) {
        if(Trigger.isInsert || Trigger.isUpdate) {
            CandidateTriggerHandler.checkExpectedSalary(Trigger.New);
            CandidateTriggerHandler.checkJobStatus(Trigger.New, Trigger.oldMap);
            CandidateTriggerHandler.insertApplicationDate(Trigger.New);
        }
    }  
}