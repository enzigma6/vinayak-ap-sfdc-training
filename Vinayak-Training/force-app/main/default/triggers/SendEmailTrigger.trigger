trigger SendEmailTrigger on Candidate_Vinayak__c (after insert, after update) {
	
    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            SendEmail.checkIfInsertingData(Trigger.New);
        }
        if(Trigger.isUpdate) {
            SendEmail.checkIfUpdatingData(Trigger.New, Trigger.oldMap);
        }
    }
}